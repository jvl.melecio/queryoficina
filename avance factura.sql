SELECT factura.folio,
				factura.serie,
				electronica.id_cliente AS client_id,
				factura.id_localidad AS location_id,
				factura.id_localidad AS branch_address_id, --  Se toma la direccion de la localiad para la direccion de la sucursal
				factura.id_localidad AS distribution_warehouse_id, -- Se toma la direccion de la localidad para la direccion del almacen
				'' AS type_internal_order,
				'' AS config_description,
				localidad.telefono_1 AS phone1,
				localidad.telefono_2 AS phone2,
				'' AS internal_order,
				UPPER(CONCAT(localidad.direccion,' ',localidad.numero_exterior,' ',localidad.numero_interior)) AS street,
				UPPER(localidad.colonia) AS col,
				UPPER(CONCAT(municipio.municipio,', ',estado.estado)) AS city,
				UPPER(CONCAT('Mexico, C.P. '||localidad.codigo_postal)) AS country,
			  '' AS bidding,
				'' AS payment_conditions,
				'' AS buy_order,
				'' AS observations,
				factura.iva AS iva_tax,
				factura.subtotal AS sub_total,
				factura.importe AS total,
				factura.descuento AS discount,
				UPPER(CONCAT(metodo_pago.metodo_pago||' - '||metodo_pago.descripcion)) AS payment_method,
				UPPER(CONCAT(forma_pago.forma_pago||' - '||forma_pago.descripcion)) AS payment_type,
				UPPER(CONCAT(uso_cfdi.uso_cfdi||' - '||uso_cfdi.descripcion)) AS cfdi_use,
				UPPER(CONCAT(moneda.moneda||' - '||moneda.descripcion)) AS currency,
				factura.tipo_cambio AS exchange_type,
				electronica.uuid AS sat_invoice_folio,
				electronica.sello_cfd AS digital_cfdi_stamp,
				electronica.sello_sat AS sat_stamp,
				electronica.fecha_timbrado AS expedition_date,
				electronica.no_certifciado_sat AS sat_certified_num,
				UPPER(CONCAT(tipo_comprobante.tipo_comprobante||' - '||tipo_comprobante.descripcion)) AS voucher_type,
				certificado.no_certificado AS certified_transmitter_num,
				UPPER(CONCAT(domicilio_fiscal.codigo_postal||', '||' '||m2.municipio||', '||edo2.estado)) AS expedition_place,
				CONCAT(electronica.version_tfd||'|'||electronica.uuid||'|'||replace(to_char(electronica.fecha_timbrado, 'MM-DD-YYYY HH24:MI:SS'),' ','T')||'|'||electronica.sello_cfd||'|'||electronica.no_certifciado_sat) AS original_string
FROM fact_factura_electronica AS electronica
INNER JOIN fact_factura AS factura
	ON factura.serie = electronica.serie
	AND factura.folio = electronica.folio_interno
INNER JOIN cat_sucursal AS sucursal
	ON factura.id_sucursal = sucursal.id_sucursal
INNER JOIN fact_domicilio_fiscal AS domicilio_fiscal
	ON sucursal.id_domicilio=domicilio_fiscal.id_domicilio
INNER JOIN cat_localidad AS localidad
	ON factura.id_localidad = localidad.id_localidad
INNER JOIN cat_estado AS estado
	ON estado.id_estado = localidad.estado::INT
INNER JOIN cat_municipio AS municipio
	ON municipio.id_municipio = localidad.municipio::INT
INNER JOIN sat_metodo_pago AS metodo_pago
	ON factura.id_metodo_pago=metodo_pago.id_metodo_pago
INNER JOIN sat_forma_pago AS forma_pago
	ON factura.id_forma_pago=forma_pago.id_forma_pago
INNER JOIN sat_uso_cfdi AS uso_cfdi
	ON uso_cfdi.id_uso_cfdi=factura.id_uso_cfdi
INNER JOIN sat_moneda AS moneda
	ON factura.id_sat_moneda=moneda.id_sat_moneda
INNER JOIN sat_tipo_comprobante AS tipo_comprobante
	ON electronica.id_tipo_comprobante=tipo_comprobante.id_tipo_comprobante
INNER JOIN cont_certificado AS certificado
	ON certificado.id_certificado=electronica.id_certificado
INNER JOIN cat_municipio AS m2
	ON domicilio_fiscal.id_municipio=m2.id_municipio
INNER JOIN cat_estado AS edo2
	ON 	domicilio_fiscal.id_estado=edo2.id_estado
WHERE electronica.uuid = '72467E6B-04E0-43AE-A403-CB8F05609CE2'


