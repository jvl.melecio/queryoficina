SELECT cliente.razon_social, cuadro_producto.id_cuadro, cuadro_producto.clave_cliente, codigo_barras.clave AS clave_abisa,codigo_barras.codigo_barras,cuadro_producto.descripcion
FROM cat_cuadro_producto AS cuadro_producto
INNER JOIN cat_producto_cliente AS producto_cliente
	ON cuadro_producto.id_cuadro = 586
	AND cuadro_producto.id_cuadro = producto_cliente.id_cuadro
	AND cuadro_producto.clave_cliente = producto_cliente.clave_cliente
INNER JOIN cat_producto_codigo_barras AS codigo_barras
	ON producto_cliente.codigo_barras = codigo_barras.codigo_barras
INNER JOIN cat_cliente AS cliente
	ON cuadro_producto.id_cliente = cliente.id_cliente
WHERE cuadro_producto.id_cuadro = 586
