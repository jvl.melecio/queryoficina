SELECT lista.id_lista, lista.descripcion, detalle.codigo_barras, detalle.precio, detalle.precio_2, detalle.precio_3, 
			CASE  WHEN detalle.estatus = 1
				THEN 'Activo'
				ELSE 'Inactivo'
			END AS status
FROM cat_lista_precio AS lista
INNER JOIN cat_lista_precio_detalle AS detalle
	ON lista.id_lista = detalle.id_lista
WHERE lista.id_lista IN (5,6)
ORDER BY lista.id_lista, detalle.codigo_barras