SELECT barras.codigo_barras, barras.nombre_comercial, barras.clave, costo_promedio(barras.codigo_barras) AS costo_promedio,
CASE  WHEN barras.tipo_producto =1 THEN 'Medicamento' 
      WHEN barras.tipo_producto =2 THEN 'Material de Curacion'
      WHEN barras.tipo_producto =8 THEN 'Abarrotes'
END as tipo,
t1.departamento, t1.categoria,
array_to_string(array_agg( t2.impuesto ), ',')  as impuestos,
precio.precio,
producto.descripcion,
producto.presentacion

FROM cat_producto_codigo_barras barras
INNER JOIN catalogo_producto producto ON producto.clave= barras.clave
LEFT JOIN (SELECT depto.descripcion as departamento , categoria.descripcion as categoria, mis.clave
 FROM cat_departamento depto INNER JOIN cat_categoria categoria ON depto.id_departamento=categoria.id_departamento 
   INNER JOIN cat_producto_miscelaneo mis  ON mis.id_categoria= categoria.id_categoria AND mis.id_departamento= depto.id_departamento ) 
                      as t1 ON t1.clave= barras.clave
LEFT JOIN (SELECT cod_impuesto.codigo_barras, impuesto.impuesto as impuesto
          FROM cat_codigo_barras_impuesto cod_impuesto 
            INNER JOIN cat_impuestos impuesto ON impuesto.id_impuesto=cod_impuesto.id_impuestos) as t2 ON t2.codigo_barras=barras.codigo_barras
LEFT JOIN cat_lista_precio_detalle precio ON precio.codigo_barras= barras.codigo_barras AND precio.id_lista=6 AND precio.estatus=1
GROUP BY barras.codigo_barras,t1.departamento,t1.categoria, precio.precio, producto.descripcion,producto.presentacion
ORDER BY barras.tipo_producto