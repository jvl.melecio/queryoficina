SELECT
	codigo.clave,
	codigo.codigo_barras,
	codigo.nombre_comercial,
	codigo.estatus,
	codigo.clave_prod_serv,
	codigo.clave_unidad,
	codigo.precio_venta,
	tipo.descripcion AS tipo,
	impuestos.impuesto,
	impuestos.porcentaje
FROM cat_producto_codigo_barras AS codigo
INNER JOIN cat_tipo_producto AS tipo
ON codigo.tipo_producto = tipo.id_tipo
LEFT JOIN cat_codigo_barras_impuesto AS codigo_impuesto
ON codigo.codigo_barras = codigo_impuesto.codigo_barras
LEFT JOIN cat_impuestos AS impuestos
ON codigo_impuesto.id_impuestos = impuestos.id_impuesto
ORDER BY codigo.codigo_barras
