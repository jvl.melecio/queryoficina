SELECT usuario.id_usuario, usuario.usuario, usuario.nombre, usuario.paterno, usuario.materno, localidad.localidad,menu.id_menu, menu.descripcion, menu.modulo,menu.url
FROM adm_usuario AS usuario
INNER JOIN adm_usuario_localidad AS usuario_localidad
	ON usuario_localidad.id_usuario = usuario.id_usuario
	AND usuario.estatus = 1
INNER JOIN cat_localidad AS localidad 
	ON localidad.id_localidad = usuario_localidad.id_localidad
	AND localidad.id_localidad = 1752
INNER JOIN adm_permiso AS permiso
	ON permiso.id_usuario = usuario.id_usuario
INNER JOIN adm_menu AS menu
	ON menu.id_menu = permiso.id_menu
	AND (menu.url IS NOT NULL ANd menu.url <> '')