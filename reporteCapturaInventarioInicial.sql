SELECT l.localidad, d.codigo_barras, (cp.descripcion || ' ' || cp.presentacion) AS descripcion,
	COALESCE(d.cantidad_toma, d.cantidad_toma_1) as cantidad_toma,
	d.no_lote, d.caducidad, d.id_ubicacion, u.nomenclatura, d.id_salida, pe.folio_resurtido, i.id_almacen, a.almacen
FROM inv_inventario i, inv_almacen a, cat_localidad l, inv_ubicacion u, cat_producto_codigo_barras pcb, catalogo_producto cp, inv_inventario_detalle d
	LEFT JOIN inv_salida s ON s.id_salida = d.id_salida
	LEFT JOIN inv_pedido_entrada pe ON pe.id_pedido = s.id_pedido AND pe.fecha_captura = s.fecha_captura
WHERE d.id_inventario = i.id_inventario
AND i.id_almacen = a.id_almacen
AND d.id_ubicacion = u.id_ubicacion
AND pcb.codigo_barras = d.codigo_barras
AND a.id_localidad = l.id_localidad
AND pcb.clave = cp.clave
AND i.tipo = '0'
AND i.estatus = '0'
--AND i.tipo_captura = '1'
AND a.id_localidad = '1823'

ORDER BY pe.id_pedido, pe.folio_resurtido, TO_ASCII(descripcion), d.no_lote, a.almacen, d.id_ubicacion