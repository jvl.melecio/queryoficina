SELECT
	i.id_inventario,
	localidad.localidad,
	A.almacen,
	CONCAT(usuario.nombre,' ',usuario.paterno,' ', usuario.materno) AS Usuario,
	i.fecha_creacion,
	A.id_almacen,
	d.id_ubicacion,
	U.nomenclatura,
	producto_cliente.clave_cliente,
	( cp.descripcion || ' ' || cp.presentacion ) AS descripcion,
	d.codigo_barras,
	d.no_lote,
	d.caducidad,
	COALESCE ( d.cantidad_toma, d.cantidad_toma_1 ) AS cantidad_toma
FROM
	inv_inventario i,
	adm_usuario usuario,
	inv_almacen A,
	inv_ubicacion U,
	cat_localidad localidad,
	cat_producto_codigo_barras pcb,
	catalogo_producto cp,
	cat_producto_cliente producto_cliente,
	cat_cuadro_producto cuadro_producto,
	inv_inventario_detalle d
	LEFT JOIN inv_salida s ON s.id_salida = d.id_salida
	LEFT JOIN inv_pedido_entrada pe ON pe.id_pedido = s.id_pedido 
	AND pe.fecha_captura = s.fecha_captura 
WHERE
	d.id_inventario = i.id_inventario 
	AND i.id_almacen = A.id_almacen 
	AND usuario.id_usuario = i.id_usuario_creacion
	AND pcb.codigo_barras = d.codigo_barras 
	AND pcb.clave = cp.clave 
	AND producto_cliente.codigo_barras = d.codigo_barras
	AND U.id_ubicacion = d.id_ubicacion
	AND i.tipo = '0' 
	AND i.estatus = '0' 
	--AND i.tipo_captura = '1' 
	AND A.id_localidad = localidad.id_localidad
	--AND a.id_localidad = '1820' -- Farmacia
	AND a.id_localidad = '1819' -- Almacen
	AND producto_cliente.clave_cliente = cuadro_producto.clave_cliente AND cuadro_producto.id_cuadro = 586
	GROUP BY	i.id_inventario,
						localidad.localidad,
						A.almacen,
						--CONCAT(
						usuario.nombre,
						usuario.paterno,
						usuario.materno,
						i.fecha_creacion,
						A.id_almacen,
						d.id_ubicacion,
						U.nomenclatura,
						producto_cliente.clave_cliente,
						--( 
						cp.descripcion,
						cp.presentacion,
						d.codigo_barras,
						d.no_lote,
						d.caducidad,
						d.cantidad_toma,
						d.cantidad_toma_1,
						pe.id_pedido,
						pe.folio_resurtido
ORDER BY pe.id_pedido, pe.folio_resurtido, d.no_lote, a.almacen, d.id_ubicacion
