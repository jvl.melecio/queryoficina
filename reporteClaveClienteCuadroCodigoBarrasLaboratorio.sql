SELECT  cuadro_producto.id_cuadro,
				cliente.id_cliente,
				cliente.razon_social,
				cliente.alias,
				cuadro.fecha_inicio,
				cuadro.fecha_fin,
				cuadro.no_evento,
				cuadro_producto.clave_cliente,
				producto_cliente.codigo_barras,
				cuadro_producto.descripcion,
				codigo_barras.nombre_comercial,
				laboratorio.laboratorio
FROM cat_cuadro AS cuadro
INNER JOIN cat_cuadro_producto AS cuadro_producto
	ON cuadro.id_cuadro = cuadro_producto.id_cuadro
INNER JOIN cat_producto_cliente AS producto_cliente
	ON cuadro_producto.id_cuadro = producto_cliente.id_cuadro
	AND cuadro_producto.clave_cliente = producto_cliente.clave_cliente
INNER JOIN cat_cliente AS cliente
	ON cliente.id_cliente = cuadro_producto.id_cliente
INNER JOIN cat_producto_codigo_barras AS codigo_barras
	ON codigo_barras.codigo_barras = producto_cliente.codigo_barras
	AND codigo_barras.estatus = 1
INNER JOIN cat_laboratorio AS laboratorio
	ON laboratorio.id_laboratorio = codigo_barras.id_laboratorio
WHERE cuadro_producto.id_cuadro = 586
ORDER BY cuadro_producto.clave_cliente
	