SELECT ingreso.id_expediente,
				ingreso.id_ingreso,
				surtido.id_surtido,
				ingreso.fecha_captura, CONCAT(expediente.nombre_paciente, ' ', expediente.paterno_paciente,' ', expediente.materno_paciente) AS paciente,
			 CONCAT(medico.nombre,' ',medico.paterno,' ',medico.materno) AS medico, medico.cedula
FROM tap_ingreso AS ingreso
INNER JOIN tap_surtido AS surtido
	ON ingreso.id_ingreso = surtido.id_ingreso
INNER JOIN expediente_hospital_tap AS expediente 
	ON expediente.folio_expediente = ingreso.id_expediente
LEFT JOIN cat_medico_hosp AS medico
	ON ingreso.id_medico = medico.id_medico
WHERE ingreso.fecha_captura > '2021-05-16 00:00:00'
ORDER BY ingreso.fecha_captura ASC