SELECT
	la.localidad AS farmacia,
	pcb.codigo_barras,
	CONCAT ( P.descripcion, ' ', P.presentacion ) AS descripcion,
	COALESCE(sql_lista.precio,0) AS p_venta,
	COALESCE(sql_costos.max_costo,0) AS p_compra,
	(100/
				CASE	WHEN sql_costos.max_costo = 0 THEN 1
							ELSE COALESCE(sql_costos.max_costo,1)
				END
				)* (COALESCE(sql_lista.precio,0) - COALESCE(sql_costos.max_costo,0)) AS margen,
	(COALESCE(sql_lista.precio,0) - COALESCE(sql_costos.max_costo,0)) AS utilidad,
	e.quedan AS existencias,
	(COALESCE(sql_lista.precio,0) - COALESCE(sql_costos.max_costo,0)) * e.quedan AS utilidad_total,
	COALESCE(sql_costos.max_costo,0) * e.quedan AS inventario_total,
	'1' AS contador,
		CASE
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_medicamento WHERE clave = pcb.clave) THEN 'MEDICAMENTO'
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_material_curacion WHERE clave = pcb.clave) THEN 'MATERIAL DE CURACION'
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_miscelaneo WHERE clave = pcb.clave) OR tp.descripcion = 'MISCELANEO' THEN d.descripcion
	END departamento,
	CASE
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_medicamento WHERE clave = pcb.clave) THEN tp.descripcion
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_material_curacion WHERE clave = pcb.clave) THEN gr.descripcion
		WHEN pcb.clave IN (SELECT clave FROM cat_producto_miscelaneo WHERE clave = pcb.clave) OR tp.descripcion = 'MISCELANEO' THEN c.descripcion
	END categoria
FROM
	inv_existencia AS e
LEFT JOIN inv_almacen AS A
	ON e.id_almacen = A.id_almacen
LEFT JOIN cat_localidad AS la
	ON e.id_localidad = la.id_localidad
LEFT JOIN adm_usuario_localidad AS ul
	ON e.id_localidad = ul.id_localidad
LEFT JOIN inv_ubicacion AS u
	ON e.id_ubicacion = u.id_ubicacion
LEFT JOIN cat_producto_codigo_barras AS pcb
	ON e.codigo_barras = pcb.codigo_barras
LEFT JOIN cat_laboratorio AS l
	ON pcb.id_laboratorio = l.id_laboratorio
LEFT JOIN catalogo_producto AS P
	ON pcb.clave = P.clave
LEFT JOIN cat_presentacion_venta AS pv
	ON pv.id_presentacion_venta = pcb.presentacion_venta
LEFT JOIN cat_tipo_producto AS tp
	ON tp.id_tipo = pcb.tipo_producto 
LEFT OUTER JOIN	cat_producto_medicamento AS pm
	ON pcb.clave = pm.clave
LEFT OUTER JOIN	cat_producto_material_curacion AS pmc
	ON pcb.clave = pmc.clave
LEFT OUTER JOIN	cat_producto_miscelaneo AS pmis
	ON pcb.clave = pmis.clave
LEFT OUTER JOIN cat_grupo AS gr
	ON pmc.id_grupo = gr.id_grupo
LEFT OUTER JOIN cat_departamento AS d
	ON pmis.id_departamento = d.id_departamento
LEFT OUTER JOIN cat_categoria AS c	
	ON pmis.id_categoria = c.id_categoria
LEFT JOIN (
		SELECT codigo_barras,no_lote,MAX(costo) AS max_costo
		FROM inv_entrada AS entrada
		GROUP BY codigo_barras,no_lote
		ORDER BY codigo_barras
	) sql_costos
	ON sql_costos.codigo_barras = e.codigo_barras
	AND sql_costos.no_lote = e.no_lote
LEFT JOIN (
					SELECT lista_detalle.codigo_barras,lista_detalle.precio,config.id_localidad
					FROM config_localidad_lista_precios AS config
					INNER JOIN cat_lista_precio AS lista_precio
						ON lista_precio.id_lista = config.general
					INNER JOIN cat_lista_precio_detalle AS lista_detalle
						ON lista_detalle.id_lista = lista_precio.id_lista
					WHERE config.id_localidad IN ( 10 )
					ORDER BY lista_detalle.codigo_barras
) sql_lista
	ON e.codigo_barras = sql_lista.codigo_barras
	AND e.id_localidad = sql_lista.id_localidad

WHERE
	ul.id_usuario =1
AND u.id_ubicacion = e.id_ubicacion
	AND a.id_localidad IN ( 10 )
	AND NOT EXISTS ( SELECT I.* FROM inv_inventario I WHERE I.id_almacen = A.id_almacen AND I.estatus = 0 ) 
ORDER BY
	P.descripcion,
	pcb.codigo_barras,
	e.no_lote,
	u.nomenclatura