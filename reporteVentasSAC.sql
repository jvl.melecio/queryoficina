SELECT 
	localidad.localidad,
	'periodo' AS periodo,
	venta_detalle.codigo_barras,
	CONCAT ( catalogo_productos.descripcion, ' ', catalogo_productos.presentacion ) AS descripcion,
	CASE
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_medicamento WHERE clave = producto_codigo_barras.clave) THEN 'MEDICAMENTO'
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_material_curacion WHERE clave = producto_codigo_barras.clave) THEN 'MATERIAL DE CURACION'
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_miscelaneo WHERE clave = producto_codigo_barras.clave) OR tipo_producto.descripcion = 'MISCELANEO' THEN d.descripcion
	END departamento,
	CASE
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_medicamento WHERE clave = producto_codigo_barras.clave) THEN tipo_producto.descripcion
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_material_curacion WHERE clave = producto_codigo_barras.clave) THEN gr.descripcion
		WHEN producto_codigo_barras.clave IN (SELECT clave FROM cat_producto_miscelaneo WHERE clave = producto_codigo_barras.clave) OR tipo_producto.descripcion = 'MISCELANEO' THEN c.descripcion
	END categoria,
	venta_detalle.cantidad,
	venta_detalle.valor_unitario_antes_impuestos AS p_venta,
	COALESCE(sql_costos.max_costo,0) AS p_compra,
	(venta_detalle.valor_unitario_antes_impuestos - COALESCE(sql_costos.max_costo,0)) AS utilidad,
	venta_detalle.importe_antes_impuestos AS venta,
	(venta_detalle.valor_unitario_antes_impuestos - COALESCE(sql_costos.max_costo,0))* venta_detalle.cantidad AS utilidad_bruta,
	COALESCE(sql_costos.max_costo,0) * venta_detalle.cantidad AS costo
FROM doc_venta AS venta
INNER JOIN doc_venta_detalle AS venta_detalle
	ON venta_detalle.id_venta = venta.id_venta
INNER JOIN cat_localidad AS localidad
	ON localidad.id_localidad = venta.id_localidad
INNER JOIN cat_producto_codigo_barras AS producto_codigo_barras
	ON venta_detalle.codigo_barras = producto_codigo_barras.codigo_barras
INNER JOIN cat_laboratorio AS laboratorio
	ON producto_codigo_barras.id_laboratorio = laboratorio.id_laboratorio
INNER JOIN catalogo_producto AS catalogo_productos
	ON producto_codigo_barras.clave = catalogo_productos.clave
INNER JOIN cat_tipo_producto AS tipo_producto
	ON tipo_producto.id_tipo = producto_codigo_barras.tipo_producto 
INNER JOIN inv_salida AS salida
	ON salida.observacion = 'PUNTO DE VENTA'
	AND CAST(salida.no_documento AS INT) = venta_detalle.id_venta
	AND salida.codigo_barras = venta_detalle.codigo_barras
	AND salida.cantidad = venta_detalle.cantidad
LEFT OUTER JOIN	cat_producto_medicamento AS pm
	ON producto_codigo_barras.clave = pm.clave
LEFT OUTER JOIN	cat_producto_material_curacion AS pmc
	ON producto_codigo_barras.clave = pmc.clave
LEFT OUTER JOIN	cat_producto_miscelaneo AS pmis
	ON producto_codigo_barras.clave = pmis.clave
LEFT OUTER JOIN cat_grupo AS gr
	ON pmc.id_grupo = gr.id_grupo
LEFT OUTER JOIN cat_departamento AS d
	ON pmis.id_departamento = d.id_departamento
LEFT OUTER JOIN cat_categoria AS c	
	ON pmis.id_categoria = c.id_categoria
LEFT JOIN (
		SELECT codigo_barras,no_lote,MAX(costo) AS max_costo
		FROM inv_entrada AS entrada
		GROUP BY codigo_barras,no_lote
		ORDER BY codigo_barras
	) sql_costos
	ON sql_costos.codigo_barras = venta_detalle.codigo_barras
	AND sql_costos.no_lote = salida.no_lote

	
WHERE venta.fecha_venta BETWEEN '2021-10-01 00:00:00' AND '2021-10-31 23:59:59'
	 AND venta.id_localidad IN ( 10 )