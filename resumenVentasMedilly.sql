SELECT
	venta.id_venta, venta.fecha_venta,venta.total_venta,venta.subtotal_venta,
	detalle.codigo_barras, detalle.descripcion,detalle.cantidad,detalle.valor_unitario,detalle.importe,detalle.valor_unitario_antes_impuestos, detalle.importe_antes_impuestos,detalle.clave_unidad,
	impuestos.base,impuestos.impuesto, impuestos.tipo_factor,impuestos.tasa_cuota,impuestos.importe, impuestos.tipo_impuesto
FROM doc_venta AS venta
INNER JOIN doc_venta_detalle AS detalle
	ON venta.id_venta = detalle.id_venta
INNER JOIN doc_venta_detalle_impuestos AS impuestos
	ON detalle.id_venta = impuestos.id_venta
	AND detalle.codigo_barras = impuestos.codigo_barras
WHERE venta.fecha_venta BETWEEN '2021-01-18 00:00:00' AND '2021-05-31 23:59:59'
AND venta.id_localidad = 10
AND venta.estatus = 1
ORDER BY fecha_venta ASC